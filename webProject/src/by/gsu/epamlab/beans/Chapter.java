package by.gsu.epamlab.beans;

public enum Chapter {
	TODAY,
	TOMORROW,
	SOMEDAY,
	FIX,
	RECYCLE_BIN
}
