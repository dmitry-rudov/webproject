package by.gsu.epamlab.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DateFormat {

    private static SimpleDateFormat formatDate;
    private static Calendar date;
    
    private static final int ONE_DAY = 1;

	private DateFormat() {
	}
	
	public static String getNowDate(String format) {
		setDateFormat(format);
		return getDate();
	}

	public static String getNextDate(String format) {
		setDateFormat(format);
		date.add(Calendar.DAY_OF_YEAR, ONE_DAY);
		return getDate();
	}
	
	
	private static void setDateFormat(String format){
		date = new GregorianCalendar();
		formatDate = new SimpleDateFormat(format);
	}
	
	private static String getDate(){
		return formatDate.format(date.getTime());
	}
	
}
