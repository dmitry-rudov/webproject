package by.gsu.epamlab.beans;

import by.gsu.epamlab.constants.Constants;

public class User {
	private String login;
	private Role role;
	
	public User() {
		login = Constants.GUEST;
		role = Role.GUEST;
	}
	
	public User(String login, Role role) {
		this.login = login;
		this.role = role;
	}
	
	public String getLogin(){
		return login;
	}
	
	public Role getRole(){
		return role;
	}

}
