package by.gsu.epamlab.beans;

import java.sql.Date;

import by.gsu.epamlab.constants.Constants;

public class Task implements Comparable<Task> {
	
	private int id; 
	private String task;
	private String file;
	private Date date;
	
	public Task(int id, String task, Date date) {
		this(id, task, Constants.EMPTY, date);
	}

	public Task(int id, String task, String file, Date date) {
		this.id = id;
		this.task = task;
		this.file = file;
		this.date = date;
	}
	
	public String getTask() {
		return task;
	}

	public String getFile() {
		return file;
	}

	public int getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	@Override
	public int compareTo(Task o) {
		return this.id - o.id;
	}

}
