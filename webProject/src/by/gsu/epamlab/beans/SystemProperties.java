package by.gsu.epamlab.beans;

public class SystemProperties {

	private static final String OS_WIN = "win";
	private static final String SEPARATOR_WIN = "\\";
	private static final String SEPARATOR_NIX_OR_NUX = "/";
	
	private static final String DIR_FILE = "web";

	private static final String PROPERTY_OS_NAME = "os.name";
	private static final String PROPERTY_HOME_PATH_USER = "user.home";

	private SystemProperties() {
	}
	
	public static String getPath(String login){
		String separatorOS = getSeparatorOS();
		return System.getProperty(PROPERTY_HOME_PATH_USER) 
			 + separatorOS + DIR_FILE + separatorOS + login + separatorOS;
	} 
	
	public static String getSeparatorOS(){
		String result = SEPARATOR_NIX_OR_NUX;
		
		String os = System.getProperty(PROPERTY_OS_NAME);
		os = os.toLowerCase();
		
		if (os.indexOf(OS_WIN) >= 0){
			result = SEPARATOR_WIN;
		} 
		
		return result;
	}
}
