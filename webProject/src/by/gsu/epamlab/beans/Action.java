package by.gsu.epamlab.beans;

public enum Action {
	FIX,
	DELETE,
	CLEAR_ALL,
	REMOVE,
	RECOVER,
	ADD_TASK,
	ADD_FILE,
	DELETE_FILE
}
