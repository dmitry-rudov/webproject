package by.gsu.epamlab.exceptions;

public class FileException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FileException() {
	}

	public FileException(String message) {
		super(message);
	}

}
