package by.gsu.epamlab.DAO.Interface;

import java.util.List;

import by.gsu.epamlab.beans.Task;
import by.gsu.epamlab.beans.User;

public interface ITaskDAO {
	public void addTask(String task, String date, User user);
	public List<Task> updateTask(User user, String chapter, String action, String[] checkbox);
	public List<Task> getListTask(User user, String chapter);
	public void addFileTask(String numberTask, String fileName);
	public void deleteFileTask(String numberTask);
}
