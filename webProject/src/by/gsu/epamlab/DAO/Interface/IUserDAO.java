package by.gsu.epamlab.DAO.Interface;

import by.gsu.epamlab.beans.User;

public interface IUserDAO {
	public User getUser(String login, String password);
	public User addUser(String login, String password);
}
