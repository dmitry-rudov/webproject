package by.gsu.epamlab.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gsu.epamlab.DAO.Interface.ITaskDAO;
import by.gsu.epamlab.beans.Action;
import by.gsu.epamlab.beans.Chapter;
import by.gsu.epamlab.beans.Task;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;
import by.gsu.epamlab.file.FileDelete;

public class DBTaskImpl implements ITaskDAO{
	
	private Connection connection;
	private PreparedStatement statementSelect;
	private PreparedStatement statementUpdate;
	private ResultSet resultSet;

	public DBTaskImpl(){
	}
	
	public void addTask(String task, String date, User user){
		try {
			int id = 0;
			
			connection = DBConnection.getDBConnection();
			statementSelect = connection.prepareStatement(Constants.QUERY_ID);
			statementSelect.setString(Constants.INDEX_QUERY_ID, user.getLogin());
			resultSet = statementSelect.executeQuery();
			
			while (resultSet.next()) {
				id = resultSet.getInt(Constants.INDEX_QUERY_ID);
			}

			statementUpdate = connection.prepareStatement(Constants.INSERT_TASK);
			statementUpdate.setInt(Constants.INDEX_INSERT_ID, id);
			statementUpdate.setString(Constants.INDEX_INSERT_TASK, task);
			statementUpdate.setDate(Constants.INDEX_INSERT_DATE, Date.valueOf(date));
			statementUpdate.executeUpdate();
			
			
		} catch (SQLException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_ADD_NEW_TASK);
		} finally {
			DBConnection.closeResult(resultSet);
			DBConnection.closeStatements(statementSelect, statementUpdate);
			DBConnection.closeConnection(connection);
		}
	}


	public List<Task> updateTask(User user, String chapter, String action, String[] numberTasks){

		action = action.replace(Constants.SPACE, Constants.UNDERSCORE);
		action = action.toUpperCase();
		
		try{
			Action actionSelect = Action.valueOf(action);
			
			switch (actionSelect) {
				case FIX: {
					updateTask(numberTasks, Constants.UPDATE_FIX);
					break;
				}
				
				case REMOVE: {
					updateTask(numberTasks, Constants.UPDATE_REMOVE);
					break;
				}
				
				case RECOVER: {
					updateTask(numberTasks, Constants.UPDATE_RECOVER);
					break;
				}
				
				case DELETE: {
					StringBuilder queryBuilder = new StringBuilder(Constants.QUERY_TASK_FOR_FILE_DELETE_BEGIN_TASK_UID); 
					int numberTaskLength = numberTasks.length - 1;
					for (int i = 0; i < numberTaskLength; i++) {
						queryBuilder.append(numberTasks[i]);
						queryBuilder.append(Constants.QUERY_TASK_FOR_FILE_DELETE_BEGIN_TASK_UID_OR);
					}
					queryBuilder.append(numberTasks[numberTaskLength]);
					queryBuilder.append(Constants.QUERY_TASK_FOR_FILE_DELETE_END);
					
					FileDelete.delete(user, getTasks(user, Constants.QUERY_TASK_FOR_FILE_DELETE_BEGIN + queryBuilder));
					deleteTask(numberTasks, Constants.DELETE);
					break;
				}

				case CLEAR_ALL: {
					FileDelete.delete(user, getTasks(user, Constants.QUERY_TASK_FOR_FILE_DELETE_ALL));
					deleteAllTask(user, Constants.DELETE_ALL);
					break;
				}

				default: {
					return new ArrayList<Task>();
				}
				
			}
		} catch(IllegalArgumentException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_ERROR_UPDATE);
		}
		
		return getListTask(user, chapter);
	}
	
	public List<Task> getListTask(User user, String chapter){
		
		chapter = chapter.replace(Constants.SPACE, Constants.UNDERSCORE);
		chapter = chapter.toUpperCase();
		String query;
		
		try{
			Chapter chapterNow = Chapter.valueOf(chapter);
			
			switch (chapterNow) {
				case TODAY: {
					query = Constants.QUERY_TASK_TODAY;
					break;
				}
				
				case TOMORROW: {
					query = Constants.QUERY_TASK_TOMORROW;
					break;
				}
				
				case SOMEDAY: {
					query = Constants.QUERY_TASK_SOMEDAY;
					break;
				}
				
				case FIX: {
					query = Constants.QUERY_TASK_FIX;
					break;
				}

				case RECYCLE_BIN: {
					query = Constants.QUERY_TASK_RECYCLEBIN;
					break;
				}
				
				default: {
					return new ArrayList<Task>();
				}
			}
		} catch(IllegalArgumentException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_ERROR_QUERY);
		}
		
		return getTasks(user, query);
	}

	private List<Task> getTasks(User user, String query) {
		try {
			List<Task> tasks = new ArrayList<Task>();
			
			connection = DBConnection.getDBConnection();
			statementSelect = connection.prepareStatement(query);
			statementSelect.setString(Constants.INDEX_QUERY_LOGIN, user.getLogin());
			resultSet = statementSelect.executeQuery();

			while (resultSet.next()) {
				tasks.add(new Task(resultSet.getInt(Constants.INDEX_QUERY_ID), 
								   resultSet.getString(Constants.INDEX_QUERY_TASK),
								   resultSet.getString(Constants.INDEX_QUERY_FILE),
								   resultSet.getDate(Constants.INDEX_QUERY_DATE)));
			}
			
			return tasks;
		} catch (SQLException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_ERROR_QUERY);
		} finally {
			DBConnection.closeResult(resultSet);
			DBConnection.closeStatements(statementSelect);
			DBConnection.closeConnection(connection);
		}
	}

	private void updateTask(String[] id, String query){
		try {
			connection = DBConnection.getDBConnection();
			statementUpdate = connection.prepareStatement(query);
			
			for (String uid : id) {
				statementUpdate.setInt(Constants.INDEX_UPDATE_UID, new Integer(uid));
				statementUpdate.executeUpdate();
			}
			
		}catch (SQLException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_ERROR_UPDATE);
		} finally {
			DBConnection.closeStatements(statementUpdate);
			DBConnection.closeConnection(connection);
		}
	}
	
	private void deleteTask(String[] id, String query){
		updateTask(id, query);
	}

	private void deleteAllTask(User user, String query){
		try {
			int id = 0;
			connection = DBConnection.getDBConnection();
			statementSelect = connection.prepareStatement(Constants.QUERY_ID);
			statementSelect.setString(Constants.INDEX_QUERY_LOGIN, user.getLogin());
			resultSet = statementSelect.executeQuery();
		
			while (resultSet.next()) {
				id = resultSet.getInt(Constants.INDEX_QUERY_ID);
			}

			statementUpdate = connection.prepareStatement(query);
			statementUpdate.setInt(Constants.INDEX_UPDATE_ID, id);
			statementUpdate.executeUpdate();
		}catch (SQLException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_ERROR_UPDATE);
		} finally {
			DBConnection.closeResult(resultSet);
			DBConnection.closeStatements(statementUpdate);
			DBConnection.closeConnection(connection);
		}
	}

	private void uploadFile(String numberTask, String fileName){
		String query = Constants.UPDATE_FILE_BEGIN + fileName + Constants.UPDATE_FILE_END;
		String[] id = {numberTask};
		updateTask(id, query);
	}
	
	@Override
	public void addFileTask(String numberTask, String fileName) {
		uploadFile(numberTask, fileName);
	}

	@Override
	public void deleteFileTask(String numberTask) {
		uploadFile(numberTask, Constants.EMPTY);
	}

}
