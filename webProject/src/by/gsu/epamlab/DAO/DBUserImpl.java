package by.gsu.epamlab.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import by.gsu.epamlab.DAO.Interface.IUserDAO;
import by.gsu.epamlab.beans.Role;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;

public class DBUserImpl implements  IUserDAO{
	
	private Connection connection;
	private PreparedStatement statementSelect;
	private PreparedStatement statementUpdate;
	private ResultSet resultSet;
	private Role role;

	public DBUserImpl() {
		
	}

	private ResultSet passwordQuery(String login) throws SQLException {
			connection = DBConnection.getDBConnection();
			statementSelect = connection.prepareStatement(Constants.QUERY_PASSWORD);
			statementSelect.setString(Constants.INDEX_QUERY_LOGIN, login);
			return statementSelect.executeQuery();
	}
	
	@Override
	public User getUser(String login, String password) {
		try {
			resultSet = passwordQuery(login);
			
			if (!resultSet.next()){
				throw new DAOException(Constants.EXCEPTION_DAO_INCORRECT_LOGIN_PASSWORD);
			}

			if (resultSet.getString(Constants.INDEX_QUERY_PASSWORD).equals(password)) {
				if (login.equals(Constants.ROOT)) {
					role = Role.ROOT;
				} else {
					role = Role.USER;
				}
				return new User(login, role);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.closeResult(resultSet);
			DBConnection.closeStatements(statementSelect);
			DBConnection.closeConnection(connection);
		}
		
		return null;
	}

	@Override
	public User addUser(String login, String password) {
		try {
			synchronized (this) {
				resultSet = passwordQuery(login);
				
				if (!resultSet.next()){
					statementUpdate = connection.prepareStatement(Constants.INSERT_NEW_USER);
					statementUpdate.setString(Constants.INDEX_INSERT_NEW_USER_LOGIN, login);
					statementUpdate.setString(Constants.INDEX_INSERT_NEW_USER_PASSWORD, password);
					statementUpdate.executeUpdate();			
					return new User(login, Role.USER);
				} else {
					throw new DAOException(Constants.EXCEPTION_DAO_LOGIN_BUSY);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.closeResult(resultSet);
			DBConnection.closeStatements(statementSelect, statementUpdate);
			DBConnection.closeConnection(connection);
		}
		
		return null;
	}

}
