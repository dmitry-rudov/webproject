package by.gsu.epamlab.DAO;

import java.util.Map;
import java.util.TreeMap;

import by.gsu.epamlab.DAO.Interface.IUserDAO;
import by.gsu.epamlab.beans.Role;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;

public class RAMUserImpl implements IUserDAO {

	private static volatile Map<String, String> users = new TreeMap<String, String>();
	private Role role = Role.GUEST;
	
	static {
		users.put(Constants.ROOT, Constants.PASSWORD_ROOT);
	}
	
	@Override
	public User getUser(String login, String password) {
		User result = new User (Constants.GUEST, role);
		if(users.containsKey(login)&&
		   users.get(login).equals(password)) {
			if (login.equals(Constants.ROOT)) {
				role = Role.ROOT;
			} else {
				role = Role.USER;
			}
			result = new User(login, role);
		} else {
			throw new DAOException(Constants.EXCEPTION_DAO_INCORRECT_LOGIN_PASSWORD);
		}
		return result;
	}

	@Override
	public User addUser(String login, String password) {
		synchronized (users) {
			User result = new User (Constants.GUEST, role);
			if (!users.containsKey(login) && !login.equals(Constants.GUEST)){
				users.put(login, password);
				result = new User(login, Role.USER);
			} else {
				throw new DAOException(Constants.EXCEPTION_DAO_LOGIN_BUSY);
			}
			return result;
		}
	}

}
