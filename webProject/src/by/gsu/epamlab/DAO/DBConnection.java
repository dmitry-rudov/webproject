package by.gsu.epamlab.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;

public class DBConnection {
	

	
	private static Class driver = null;
	
	private DBConnection() {
		try {
			driver = Class.forName(Constants.DB_CLASS_NAME);
		} catch (ClassNotFoundException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_DATABASE_CONNECTION);
		} 
	}

	private synchronized static void getDBDriver() {
		if (driver == null) {
			new DBConnection();
		}
	}
	
	public static Connection getDBConnection() throws SQLException {
		if (driver == null) {
			getDBDriver();
		}
		
		return DriverManager.getConnection(Constants.DB_CONNECTION, Constants.DB_USER, Constants.DB_PASSWORD);
	}
	
	public static void closeResult(ResultSet ... results){
		for (ResultSet rs : results){
			try {
				if (rs != null){
					rs.close();
				}
			} catch (SQLException e) {
				throw new DAOException(Constants.EXCEPTION_DAO_DATABASE_CONNECTION);
			}	
		}
	}
	
	public static void closeStatements(Statement ... statements){
		for (Statement st : statements){
			try {
				if (st != null){
					st.close();
				}
			} catch (SQLException e) {
				throw new DAOException(Constants.EXCEPTION_DAO_DATABASE_CONNECTION);
			}
		}
	}
	
	public static void closeConnection(Connection cn){
		try {
			if (cn != null) {
				cn.close();
			}
		} catch (SQLException e) {
			throw new DAOException(Constants.EXCEPTION_DAO_DATABASE_CONNECTION);
		}
	}

}
