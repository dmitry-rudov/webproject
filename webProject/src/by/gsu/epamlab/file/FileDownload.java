package by.gsu.epamlab.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.FileException;

public class FileDownload {

	private FileDownload() {
	}
	
	public static void download(File downloadFile, OutputStream oStream){
		FileInputStream fiStream = null;
		try{
			fiStream = new FileInputStream(downloadFile);
			
			byte[] buffer = new byte[Constants.BUFFER_SIZE];
			int bytesRead = -1;
			
			while ((bytesRead = fiStream.read(buffer)) != -1) {
				oStream.write(buffer, 0, bytesRead);
			}
			
		} catch (IOException e) {
			throw new FileException(Constants.EXCEPTION_FILE_DOWNLOAD);
		} finally {
			try {
				if (fiStream != null) {
					fiStream.close();
				}
			} catch (IOException e) {
				throw new FileException(Constants.EXCEPTION_FILE_DOWNLOAD);
			}
		}
	}

}
