package by.gsu.epamlab.file;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import by.gsu.epamlab.beans.SystemProperties;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.FileException;

public class FileUpload {
	
	private static final int INDEX_END_OFFSET = 4;
	
	private static final String PARAMETR_FILE_NAME = "filename=\"";
	private static final String PARAMETR_FILE_NAME_FOR_SERVER = "Upload\"\r\n\r\n";
	private static final String SIMBOL_CR = "\n";
	private static final String SPECIAL_SIMBOLS_END_FILE_NAME = "\"\r\n";
	private static final String EMPTY_STRING = "";
	private static final String EQUALS = "=";
	
	private byte[] arrayData;
	private String fileName;
	private String numberTask;
	
	
	private final String boundary;
	private final String path;

	public FileUpload(String contentType, int contentLength, InputStream iStream, String login) {
		path = SystemProperties.getPath(login);
		boundary = getBoundary(contentType); 
		arrayData = getArrayData(iStream, contentLength);
	}
	
	private byte[] getArrayData(InputStream iStream, int contentLength) {

		DataInputStream diStream = new DataInputStream(iStream);
 	 	byte array[] = new byte[contentLength];
 	 	int dataRead = 0;
 	 	int totalData = 0;

 	 	try{
	 	 	while (totalData < contentLength){
	 	 	 	dataRead = diStream.read(array, totalData, contentLength);
	 	 	 	totalData += dataRead;
	 	 	}
		} catch (IOException e){
			throw new FileException(Constants.EXCEPTION_FILE_UPLOAD);
		} finally {
			try {
				diStream.close();
			} catch (IOException e) {
				throw new FileException(Constants.EXCEPTION_FILE_UPLOAD);
			}
		}
 	 	
 	 	return array;
	}
	
	public void upload() {
		String fileName = getNumberTask();
		String dataString = new String(arrayData);

 	 	if (fileName.length() > 0) {
 			int position = dataString.indexOf(PARAMETR_FILE_NAME);
 	 	 	position = dataString.indexOf(SIMBOL_CR, position) + 1;
 	 	 	position = dataString.indexOf(SIMBOL_CR, position) + 1;
 	 	 	position = dataString.indexOf(SIMBOL_CR, position) + 1;
 	 	 	
 	 	 	String fileData = dataString.substring(position, dataString.indexOf(boundary, position) - INDEX_END_OFFSET);
 	 	 	if (fileData.length() > 0) {
 	 	 	 	FileOutputStream foStream = null;
 	 	 	 	writeDir();
				try {
					foStream = new FileOutputStream(path + fileName);
	 	 	 	 	foStream.write (arrayData, position, fileData.length());
				} catch (IOException e) {
					throw new FileException(Constants.EXCEPTION_FILE_UPLOAD);
				} finally {
	 	 	 	 	try {
	 	 	 	 		if (foStream != null) {
							foStream.close();
	 	 	 	 		}
					} catch (IOException e) {
						throw new FileException(Constants.EXCEPTION_FILE_UPLOAD);
					}
				}
 	 	 	}
 	 	}
	}
	
	public String getFileName(){
		
		if (fileName == null){
	 	 	fileName = EMPTY_STRING;
	 	 	String dataString = new String(arrayData);

	 	 	int position = dataString.indexOf(PARAMETR_FILE_NAME);
	 	 	int positionStart = position + PARAMETR_FILE_NAME.length();
		 	
	 	 	position = dataString.indexOf(SIMBOL_CR, position) + 1;
		 	int positionEnd = position - (SPECIAL_SIMBOLS_END_FILE_NAME.length() + 1);
		 	 
	 
	 	 	if ((positionStart > 0) && (positionStart < positionEnd)) {
	 	 		fileName = dataString.substring(positionStart, positionEnd + 1);
	 	 	} else {
				throw new FileException(Constants.EXCEPTION_FILE_UPLOAD);
	 	 	}
			
		}
 	 	
 	 	return fileName;
	}
	
	public String getNumberTask(){
		
 	 	if (numberTask == null) {
 			numberTask = EMPTY_STRING;

 	 	 	String dataString = new String(arrayData);

 	 	 	int position = dataString.indexOf(PARAMETR_FILE_NAME_FOR_SERVER) + 1;
 	 	 	int positionStart = position + PARAMETR_FILE_NAME.length();
 	 	 	
 	 	 	position =  dataString.indexOf(boundary, position) - 1;
 		 	
 	 	 	int positionEnd = position - INDEX_END_OFFSET;
 		 	 
 	 
 	 	 	if ((positionStart > 0) && (positionStart < positionEnd)) {
 	 	 		numberTask = dataString.substring(positionStart, positionEnd + 1);
 	 	 	} else {
 				throw new FileException(Constants.EXCEPTION_FILE_UPLOAD);
 	 	 	}
 	 	}
 	 	
 	 	return numberTask;
	}

	private String getBoundary(String contentType){
 	 	int lastIndex = contentType.lastIndexOf(EQUALS);
 	 	return contentType.substring (lastIndex + 1, contentType.length());	
	}

	private void writeDir(){
		File pathFile = new File(path);
		pathFile.mkdir();
	}
	
}
