package by.gsu.epamlab.file;

import java.io.File;
import java.util.List;

import by.gsu.epamlab.beans.SystemProperties;
import by.gsu.epamlab.beans.Task;
import by.gsu.epamlab.beans.User;

public class FileDelete {

	private FileDelete() {
	}

	public static void delete(User user, String idTask) {
		String filePath = SystemProperties.getPath(user.getLogin()) + idTask;
		File deleteFile = new File(filePath);
		deleteFile.delete();
	}

	public static void delete(User user, List<Task> tasks) {
		for (Task task : tasks) {
			String filePath = SystemProperties.getPath(user.getLogin()) + task.getId();
			File deleteFile = new File(filePath);
			deleteFile.delete();
		}
	}
}
