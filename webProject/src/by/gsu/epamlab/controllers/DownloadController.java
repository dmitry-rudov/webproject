package by.gsu.epamlab.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.beans.SystemProperties;
import by.gsu.epamlab.beans.Task;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.FileException;
import by.gsu.epamlab.file.FileDownload;

/**
 * Servlet implementation class DownloadController
 */
public class DownloadController extends AbstractController {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see AbstractController#AbstractController()
     */
    public DownloadController() {
        super();
    }

	@Override
	protected void performTask(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute(Constants.PARAMETR_SESSION_USER);
		String fileName = request.getParameter(Constants.PARAMETR_REQUEST_DOWNLOAD);
		List<Task> tasks = (List<Task>) session.getAttribute(Constants.PARAMETR_SESSION_TASKS);
		Collections.sort(tasks);
		Task taskEtalon = new Task(new Integer(fileName), null, null);
		Task taskSearch = tasks.get(Collections.binarySearch(tasks, taskEtalon));
		String realFileName = taskSearch.getFile();
		
		String filePath = SystemProperties.getPath(user.getLogin()) + fileName;
		File downloadFile = new File(filePath);
		ServletContext context = getServletContext();
		String mimeType = context.getMimeType(filePath);
		if (mimeType == null) {			
			mimeType = Constants.MIME_TYPE_DOWNLOAD_FILE_DEFAULT;
		}
		String headerKey = Constants.HEADER_KEY;
		String headerValue = String.format(Constants.HEADER_VALUE_FORMAT, realFileName);

		response.setHeader(headerKey, headerValue);
		response.setContentType(mimeType);
		response.setContentLength((int) downloadFile.length());
		
		try{
			FileDownload.download(downloadFile, response.getOutputStream());
		} catch (IOException | FileException e) {
			jumpError (e.getMessage(), Constants.TASK_SERVLET, request, response);
		}
		jump(Constants.TASK_SERVLET, request, response);
	}


}
