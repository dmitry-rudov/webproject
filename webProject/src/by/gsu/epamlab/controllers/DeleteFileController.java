package by.gsu.epamlab.controllers;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.DAO.Interface.ITaskDAO;
import by.gsu.epamlab.beans.SystemProperties;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;
import by.gsu.epamlab.exceptions.ValidationException;
import by.gsu.epamlab.factory.TaskFactory;
import by.gsu.epamlab.file.FileDelete;

/**
 * Servlet implementation class DeleteFileController
 */
public class DeleteFileController extends AbstractController {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see AbstractController#AbstractController()
     */
    public DeleteFileController() {
        super();
    }

	@Override
	protected void performTask(HttpServletRequest request,
			HttpServletResponse response) {
		try{
			HttpSession session = request.getSession(true);
			User user = (User) session.getAttribute(Constants.PARAMETR_SESSION_USER);
			String chapter = (String) session.getAttribute(Constants.PARAMETR_SESSION_TAB_SHEET);
			String idTask = request.getParameter(Constants.PARAMETR_REQUEST_DELETE_FILE);

			FileDelete.delete(user, idTask);
			
			ITaskDAO taskDAO = TaskFactory.getClassFromFactory();
			taskDAO.deleteFileTask(idTask);
			session.setAttribute(Constants.PARAMETR_SESSION_TASKS, taskDAO.getListTask(user, chapter));
			jump(Constants.TASK_SERVLET, request, response);
		} catch (ValidationException | DAOException e) {
			jumpError (e.getMessage(), Constants.TASK_SERVLET, request, response);
		}
	}

}
