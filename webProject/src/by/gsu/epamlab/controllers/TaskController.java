package by.gsu.epamlab.controllers;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.DAO.Interface.ITaskDAO;
import by.gsu.epamlab.beans.DateFormat;
import by.gsu.epamlab.beans.Task;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;
import by.gsu.epamlab.exceptions.ValidationException;
import by.gsu.epamlab.factory.TaskFactory;

/**
 * Servlet implementation class TaskController
 */
public class TaskController extends AbstractController {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see AbstractController#AbstractController()
     */
    public TaskController() {
        super();
    }

	@Override
	protected void performTask(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(true);
		
		User user = (User) session.getAttribute(Constants.PARAMETR_SESSION_USER);
		String chapter = request.getParameter(Constants.PARAMETR_REQUEST_CHAPTER);
        String date = DateFormat.getNowDate(Constants.FORMAT_DATE_FOR_PAGE);
        
		try {
			
			if (chapter != null) {
				session.setAttribute(Constants.PARAMETR_SESSION_TAB_SHEET, chapter);
				ITaskDAO taskDAO = TaskFactory.getClassFromFactory();
				List<Task> tasks = taskDAO.getListTask(user, chapter);
				session.setAttribute(Constants.PARAMETR_SESSION_TASKS, tasks);
				if (Constants.CHAPTER_TOMORROW.equals(chapter)){
					date = DateFormat.getNextDate(Constants.FORMAT_DATE_FOR_PAGE);
				}
				session.setAttribute(Constants.PARAMETR_SESSION_DATE, date);
			}
			jump(Constants.TASK_PAGE, request, response);
		} catch (ValidationException | DAOException e) {
			jumpError (e.getMessage(), Constants.TASK_PAGE, request, response);
		}
		
	}

}
