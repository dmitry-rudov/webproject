package by.gsu.epamlab.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.DAO.Interface.ITaskDAO;
import by.gsu.epamlab.beans.DateFormat;
import by.gsu.epamlab.beans.Task;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;
import by.gsu.epamlab.exceptions.ValidationException;
import by.gsu.epamlab.factory.TaskFactory;

/**
 * Servlet implementation class AddTaskController
 */
public class AddTaskController extends AbstractController {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see AbstractController#AbstractController()
     */
    public AddTaskController() {
        super();
    }

	@Override
	protected void performTask(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);
		
		User user = (User) session.getAttribute(Constants.PARAMETR_SESSION_USER);
		String chapter = (String) session.getAttribute(Constants.PARAMETR_SESSION_TAB_SHEET);
		String add = request.getParameter(Constants.PARAMETR_REQUEST_NEW_TASK_ADD);
		String cancel = request.getParameter(Constants.PARAMETR_REQUEST_NEW_TASK_CANCEL);
		String date = request.getParameter(Constants.PARAMETR_REQUEST_NEW_TASK_DATE);
		String task = request.getParameter(Constants.PARAMETR_REQUEST_NEW_TASK_TASK);

		
		try {
			if (cancel == null) {
				checkInput(add, task, date, chapter);
				if ((date == null || Constants.EMPTY.equals(date)) && Constants.CHAPTER_TODAY.equals(chapter)) {
					date = DateFormat.getNowDate(Constants.FORMAT_DATE_FOR_SQL);
				} 
				if ((date == null || Constants.EMPTY.equals(date)) && Constants.CHAPTER_TOMORROW.equals(chapter)) {
					date = DateFormat.getNextDate(Constants.FORMAT_DATE_FOR_SQL);
				}
				ITaskDAO taskDAO = TaskFactory.getClassFromFactory();
				taskDAO.addTask(task, date, user);

				List<Task> tasks = taskDAO.getListTask(user, chapter);
				session.setAttribute(Constants.PARAMETR_SESSION_TASKS, tasks);
			}

			response.sendRedirect(Constants.DIR_PROJECT + Constants.TASK_SERVLET);
		} catch (ValidationException | DAOException | IOException e) {
			jumpError (e.getMessage(), Constants.ADD_TASK_PAGE, request, response);
		}

	}

	private void checkInput(String add, String task, String date, String chapter) {

		if (add != null && task == null) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_NULL_TASK);
		}
		if (task.length() > 256) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_LENGTH_TASK);
		}
		if (Constants.EMPTY.equals(task)) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_NULL_TASK);
		}
		if (date == null && chapter.equals(Constants.CHAPTER_SOMEDAY)) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_NULL_DATE);
		}
	}


}
