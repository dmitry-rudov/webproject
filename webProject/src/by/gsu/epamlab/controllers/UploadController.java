package by.gsu.epamlab.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.DAO.Interface.ITaskDAO;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.FileException;
import by.gsu.epamlab.factory.TaskFactory;
import by.gsu.epamlab.file.FileUpload;

/**
 * Servlet implementation class UploadController
 */
public class UploadController extends AbstractController {
	private static final long serialVersionUID = 1L;
 
    /**
     * @see AbstractController#AbstractController()
     */
    public UploadController() {
        super();
    }

	@Override
	protected void performTask(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute(Constants.PARAMETR_SESSION_USER);
		String chapter = (String) session.getAttribute(Constants.PARAMETR_SESSION_TAB_SHEET);
	 	String contentType = request.getContentType();

 	 	try {
 	 	 	FileUpload file = new FileUpload(contentType, request.getContentLength(), request.getInputStream(), user.getLogin());
	 	 	file.upload();

	 	 	ITaskDAO taskDAO = TaskFactory.getClassFromFactory();
			taskDAO.addFileTask(file.getNumberTask(), file.getFileName());
			session.setAttribute(Constants.PARAMETR_SESSION_TASKS, taskDAO.getListTask(user, chapter));
		} catch(IOException | FileException e) {
			jumpError (e.getMessage(), Constants.TASK_SERVLET, request, response);
		} 	
		jump(Constants.TASK_SERVLET, request, response);
	}
	
}
