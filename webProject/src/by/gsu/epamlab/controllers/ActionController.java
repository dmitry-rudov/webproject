package by.gsu.epamlab.controllers;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.DAO.Interface.ITaskDAO;
import by.gsu.epamlab.beans.Task;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;
import by.gsu.epamlab.exceptions.ValidationException;
import by.gsu.epamlab.factory.TaskFactory;

/**
 * Servlet implementation class ActionController
 */
public class ActionController extends AbstractController {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see AbstractController#AbstractController()
     */
    public ActionController() {
        super();
    }

	@Override
	protected void performTask(HttpServletRequest request,HttpServletResponse response) {
		
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute(Constants.PARAMETR_SESSION_USER);
		String action = request.getParameter(Constants.PARAMETR_REQUEST_ACTION);
		String upload = request.getParameter(Constants.PARAMETR_REQUEST_UPLOAD);
		String download = request.getParameter(Constants.PARAMETR_REQUEST_DOWNLOAD);
		String fileDelete = request.getParameter(Constants.PARAMETR_REQUEST_DELETE_FILE);
		String chapter = (String) session.getAttribute(Constants.PARAMETR_SESSION_TAB_SHEET);
		String[] checkbox = request.getParameterValues(Constants.PARAMETR_REQUEST_TASK);
		String page;
		
		try {
			checkInput(action, upload, download, fileDelete, checkbox);
			
			page = setPage(action, upload, download, fileDelete);

			if (Constants.TASK_SERVLET.equals(page)){
				ITaskDAO taskDAO = TaskFactory.getClassFromFactory();

				List<Task> tasks = taskDAO.updateTask(user, chapter, action, checkbox);
				session.setAttribute(Constants.PARAMETR_SESSION_TASKS, tasks);
			}
			
			if (Constants.UPLOAD_PAGE.equals(page)){
				request.setAttribute(Constants.PARAMETR_REQUEST_NUMBER_TASK, upload);
			}
			
			jump(page, request, response);
		} catch (ValidationException | DAOException e) {
			jumpError (e.getMessage(), Constants.TASK_SERVLET, request, response);
		}
	}

	private String setPage(String action, String upload, String download, String fileDelete) {
		String result = Constants.TASK_SERVLET;
		

		if (Constants.ACTION_ADD_TASK.equals(action)){
			result = Constants.ADD_TASK_PAGE;
		}
		if ((upload != null)||(Constants.EMPTY.equals(upload))){
			result = Constants.UPLOAD_PAGE;
		}
		if ((download != null)||(Constants.EMPTY.equals(download))){
			result = Constants.DOWNLOAD_SERVLET;
		}
		if ((fileDelete != null)||(Constants.EMPTY.equals(fileDelete))){
			result = Constants.DELETE_FILE_SERVLET;
		}
		
		return result;
	}

	private void checkInput(String action, String upload, String download, String fileDelete, String[] checkbox) {
		if ((checkbox == null || action == null)&&
			!(Constants.ACTION_ADD_TASK.equals(action))&&
			!(Constants.ACTION_CLEAR_ALL.equals(action))&&
			!(upload != null)&&
			!(download != null)&&
			!(fileDelete != null)) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_NULL_CHECKBOX);
		}
	}

}
