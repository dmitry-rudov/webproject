package by.gsu.epamlab.controllers;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.DAO.Interface.IUserDAO;
import by.gsu.epamlab.beans.User;
import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.exceptions.DAOException;
import by.gsu.epamlab.exceptions.ValidationException;
import by.gsu.epamlab.factory.UserFactory;

/**
 * Servlet implementation class RegistrateController
 */
public class RegistrateController extends AbstractController {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrateController() {
        super();
    }

	@Override
	protected void performTask(HttpServletRequest request,	HttpServletResponse response) {
		String login = request.getParameter(Constants.PARAMETR_REQUEST_LOGIN);
		String password = request.getParameter(Constants.PARAMETR_REQUEST_PASSWORD);
		String[] passwords = request.getParameterValues(Constants.PARAMETR_REQUEST_PASSWORD);
		try {
			checkInput(login, password, passwords);
			IUserDAO userDAO = UserFactory.getClassFromFactory();
			User user = userDAO.addUser(login.trim(), password);
			HttpSession session = request.getSession();
			session.setAttribute(Constants.USER, user);
			jump(Constants.TASK_SERVLET, request, response);
		} catch (ValidationException | DAOException e) {
			jumpError (e.getMessage(), Constants.REGISTRATE_PAGE, request, response);
		}
	}

	private void checkInput(String login, String password, String[] passwords) {
		if (login == null || password == null){
			throw new ValidationException(Constants.EMPTY);
		}

		Pattern pattern = Pattern.compile(Constants.REGEX_LOGIN_PASSWORD);
		Matcher matcherLogin = pattern.matcher(login);
		Matcher matcherPassword = pattern.matcher(password);

		
		if (!matcherLogin.matches()) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_LOGIN);
		}
		
		if (!matcherPassword.matches()) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_PASSWORD);
		}
		
		if (!passwords[Constants.PASSWORD_ONE].equals(passwords[Constants.PASSWORD_TWO])) {
			throw new ValidationException(Constants.EXCEPTION_VALIDATION_PASSWORD_INCORRECT);
		}
	}




}
