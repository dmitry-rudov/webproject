package by.gsu.epamlab.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.gsu.epamlab.constants.Constants;

public abstract class AbstractController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public AbstractController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected abstract void performTask(HttpServletRequest request, HttpServletResponse response);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}
	
	protected void jumpError(String message, String url,
			HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute(Constants.KEY_ERROR, message);
		jump(url, request, response);
	}

	protected void jump(String url, HttpServletRequest request,
			HttpServletResponse response) {
	    RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
	    try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			e.printStackTrace();
		}
	}
	

}