package by.gsu.epamlab.factory;

import by.gsu.epamlab.DAO.DBTaskImpl;
import by.gsu.epamlab.DAO.Interface.ITaskDAO;

public class TaskFactory {

	private TaskFactory() {
	}
	
	public static ITaskDAO getClassFromFactory() {
		return new DBTaskImpl();
	}
}
