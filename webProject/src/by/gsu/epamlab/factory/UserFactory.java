package by.gsu.epamlab.factory;

import by.gsu.epamlab.DAO.DBUserImpl;
import by.gsu.epamlab.DAO.Interface.IUserDAO;

public class UserFactory {

	private UserFactory(){
	}
	
	public static IUserDAO getClassFromFactory() {
		return new DBUserImpl();
	}
}
