package by.gsu.epamlab.constants;

public class Constants {
	public static final int PASSWORD_ONE = 0;
	public static final int PASSWORD_TWO = 1;
	public static final int BUFFER_SIZE = 4096;

	public static final char SPACE = ' ';
	public static final char UNDERSCORE = '_';

	public static final String GUEST = "Guest";
	public static final String ROOT = "root";
	public static final String USER = "user";

	public static final String LOGIN = "login";
	public static final String ROLE = "role";
	
	public static final String PASSWORD_ROOT = "toor";
	
	public static final String EMPTY = "";
	public static final String REGEX_LOGIN_PASSWORD = "^[a-zA-Z0-9]{3,20}+$";
	
	public static final String MIME_TYPE_DOWNLOAD_FILE_DEFAULT = "application/octet-stream";
	public static final String HEADER_KEY = "Content-Disposition";
	public static final String HEADER_VALUE_FORMAT = "attachment; filename=\"%s\"";
			
	public static final String PARAMETR_REQUEST_LOGIN = "login";	
	public static final String PARAMETR_REQUEST_PASSWORD = "password";	
	public static final String PARAMETR_REQUEST_PASSWORD_REPEAT = "password_repaet";	
	public static final String PARAMETR_REQUEST_CHAPTER = "Chapter";	
	public static final String PARAMETR_REQUEST_ACTION = "Action";	
	public static final String PARAMETR_REQUEST_TASK = "task";
	public static final String PARAMETR_REQUEST_NUMBER_TASK = "number_task";
	public static final String PARAMETR_REQUEST_DOWNLOAD = "Download";
	public static final String PARAMETR_REQUEST_UPLOAD = "Upload";
	public static final String PARAMETR_SESSION_USER = "user"; 
	public static final String PARAMETR_SESSION_TAB_SHEET = "tab_sheet";
	public static final String PARAMETR_SESSION_TASKS = "tasks";
	public static final String PARAMETR_SESSION_DATE = "date";
	public static final String PARAMETR_REQUEST_NEW_TASK_ADD = "Add";
	public static final String PARAMETR_REQUEST_NEW_TASK_CANCEL = "Cancel";
	public static final String PARAMETR_REQUEST_NEW_TASK_DATE = "Calendar";	
	public static final String PARAMETR_REQUEST_NEW_TASK_FILE = "File";	
	public static final String PARAMETR_REQUEST_NEW_TASK_TASK = "Task";	
	public static final String PARAMETR_REQUEST_DELETE_FILE = "Delete file";

	public static final String ACTION_ADD_TASK = "Add task";	
	public static final String ACTION_FIX = "Fix";	
	public static final String ACTION_REMOVE = "Remove";	
	public static final String ACTION_CLEAR_ALL = "Clear all";	
	public static final String ACTION_RECOVER = "Recover";	
	public static final String ACTION_DELETE = "Delete";	
	public static final String ACTION_UPLOAD = "Upload";	
	public static final String ACTION_DOWNLOAD = "Download";	
	public static final String ACTION_FILE_DELETE = "Delete file";	
		
	
	public static final String KEY_ERROR = "error";	
	
	public static final String DIR_PROJECT = "/webProject"; 
	public static final String REGISTRATE_PAGE = "/registrate.jsp"; 
	public static final String LOGIN_PAGE = "/login.jsp"; 
	public static final String MAIN_PAGE = "/task.jsp"; 
	public static final String TASK_PAGE = "/task.jsp"; 
	public static final String UPLOAD_PAGE = "/upload.jsp"; 
	public static final String DOWNLOAD_SERVLET = "/operation/download"; 
	public static final String DELETE_FILE_SERVLET = "/operation/deleteFile"; 
	public static final String TASK_SERVLET = "/operation/task"; 
	public static final String ADD_TASK_PAGE = "/addtask.jsp"; 

	
	public static final int INDEX_QUERY_PASSWORD = 1;
	public static final int INDEX_QUERY_LOGIN = 1;
	public static final int INDEX_INSERT_NEW_USER_LOGIN = 1;
	public static final int INDEX_INSERT_NEW_USER_PASSWORD = 2;
	public static final int INDEX_QUERY_ID = 1;
	public static final int INDEX_QUERY_TASK = 2;
	public static final int INDEX_QUERY_FILE = 3;
	public static final int INDEX_QUERY_DATE = 4;
	public static final int INDEX_INSERT_ID = 1;
	public static final int INDEX_INSERT_TASK = 2;
	public static final int INDEX_INSERT_DATE = 3;
	public static final int INDEX_UPDATE_UID = 1;
	public static final int INDEX_UPDATE_ID = 1;
	
	public static final String QUERY_PASSWORD = "SELECT password FROM users WHERE user = ?;"; 
	public static final String QUERY_ID = "SELECT ID FROM users WHERE user = ?;"; 
	public static final String INSERT_NEW_USER = "INSERT INTO users (user, password) VALUES(?, ?);"; 
	public static final String INSERT_TASK = "INSERT INTO tasks (ID, Task, Date, Fix, Recycle) VALUES(?, ?, ?, 0, 0);"; 
	
	public static final String QUERY_TASK_TODAY = "SELECT tasks.UID, tasks.Task, tasks.File, tasks.Date "
												+ "FROM tasks , users "
												+ "WHERE users.user = ? "
												+ "AND users.ID = tasks.ID "
												+ "AND tasks.Date <= CURDATE() "
												+ "AND tasks.Fix = 0 "
												+ "AND tasks.Recycle = 0;"; 

	public static final String QUERY_TASK_TOMORROW = "SELECT tasks.UID, tasks.Task, tasks.File, tasks.Date "
												   + "FROM tasks , users "
												   + "WHERE users.user = ? "
												   + "AND users.ID = tasks.ID "
												   + "AND tasks.Date = DATE_ADD(CURDATE(), Interval 1 DAY) "
												   + "AND tasks.Fix = 0 "
												   + "AND tasks.Recycle = 0;"; 

	public static final String QUERY_TASK_SOMEDAY = "SELECT tasks.UID, tasks.Task, tasks.File, tasks.Date "
												  + "FROM tasks , users "
												  + "WHERE users.user = ? "
												  + "AND users.ID = tasks.ID "
												  + "AND tasks.Date > DATE_ADD(CURDATE(), Interval 1 DAY) "
												  + "AND tasks.Fix = 0 "
												  + "AND tasks.Recycle = 0;"; 

	public static final String QUERY_TASK_FIX = "SELECT tasks.UID, tasks.Task, tasks.File, tasks.Date "
											  + "FROM tasks , users "
											  + "WHERE users.user = ? "
											  + "AND users.ID = tasks.ID "
											  + "AND tasks.Fix = 1 "
											  + "AND tasks.Recycle = 0;"; 

	public static final String QUERY_TASK_RECYCLEBIN = "SELECT tasks.UID, tasks.Task, tasks.File, tasks.Date "
			  										 + "FROM tasks , users "
			  										 + "WHERE users.user = ? "
			  										 + "AND users.ID = tasks.ID "
			  										 + "AND tasks.Recycle = 1;"; 
	
	public static final String QUERY_TASK_FOR_FILE_DELETE_ALL = "SELECT tasks.UID, tasks.Task, tasks.File, tasks.Date "
															  + "FROM tasks , users "
															  + "WHERE users.user = ? "
															  + "AND tasks.File != ''"
															  + "AND tasks.Recycle = 1;";

	public static final String QUERY_TASK_FOR_FILE_DELETE_BEGIN = "SELECT tasks.UID, tasks.Task, tasks.File, tasks.Date "
												   		  + "FROM tasks , users "
												   		  + "WHERE users.user = ? "
												   		  + "AND tasks.File != ''"
												   		  + "AND ";
	public static final String QUERY_TASK_FOR_FILE_DELETE_BEGIN_TASK_UID = "(tasks.UID = ";
	public static final String QUERY_TASK_FOR_FILE_DELETE_BEGIN_TASK_UID_OR = " OR tasks.UID = ";
	public static final String QUERY_TASK_FOR_FILE_DELETE_END = ");";

	
	public static final String UPDATE_FIX = "UPDATE tasks SET Fix = 1 WHERE UID = ?";
	public static final String UPDATE_REMOVE = "UPDATE tasks SET Recycle = 1 WHERE UID = ?";
	public static final String UPDATE_RECOVER = "UPDATE tasks SET Recycle = 0 WHERE UID = ?";
	public static final String UPDATE_ADD_FILE = "UPDATE tasks SET File = ? WHERE UID = ?";
	public static final String DELETE = "DELETE FROM tasks WHERE UID = ? AND Recycle = 1";
	public static final String DELETE_ALL = "DELETE FROM tasks WHERE ID = ? AND Recycle = 1";
	public static final String UPDATE_FILE_BEGIN = "UPDATE tasks SET File = '";
	public static final String UPDATE_FILE_END = "' WHERE UID = ?";

	public static final String EXCEPTION_DAO_INCORRECT_LOGIN_PASSWORD = "Incorrect login or password"; 
	public static final String EXCEPTION_DAO_LOGIN_BUSY = "Login busy"; 
	public static final String EXCEPTION_DAO_ADD_NEW_TASK = "Error when adding a new task"; 
	public static final String EXCEPTION_DAO_ERROR_QUERY = "Error when reading data base";
	public static final String EXCEPTION_DAO_ERROR_UPDATE = "Error when updating data base";
	public static final String EXCEPTION_DAO_DATABASE_CONNECTION = "Error occurred while accessing the database";
	public static final String EXCEPTION_VALIDATION_LOGIN = "Login must be greater than 3 and less than 20 characters. \n"
																 + "The username must contain only latin letters and numbers"; 
	public static final String EXCEPTION_VALIDATION_PASSWORD = "Password must be greater than 3 and less than 20 characters. \n"
																 + "The password must contain only latin letters and numbers"; 
	public static final String EXCEPTION_VALIDATION_PASSWORD_INCORRECT = "The password is not confirmed";
	public static final String EXCEPTION_VALIDATION_LENGTH_TASK = "Length of the task must be less than 256 characters";
	public static final String EXCEPTION_VALIDATION_NULL_TASK = "The length of the task must be greater than 0 characters";
	public static final String EXCEPTION_VALIDATION_NULL_DATE = "Select dates";
	public static final String EXCEPTION_VALIDATION_NULL_CHECKBOX = "Select tasks for action";
	public static final String EXCEPTION_FILE_UPLOAD = "Error uploading file";
	public static final String EXCEPTION_FILE_DOWNLOAD = "Error downloading file";
	
	public static final String DB_CLASS_NAME = "org.gjt.mm.mysql.Driver";
	public static final String DB_CONNECTION = "jdbc:mysql://localhost/jee_dima";
	public static final String DB_USER = "jee";
	public static final String DB_PASSWORD = "jee"; 
	
	public static final String CHAPTER_TODAY = "Today"; 
	public static final String CHAPTER_TOMORROW = "Tomorrow"; 
	public static final String CHAPTER_SOMEDAY = "Someday"; 
	public static final String CHAPTER_FIX = "Fix"; 
	public static final String CHAPTER_RECYCLED_BIN = "Recycle Bin"; 

	public static final String FORMAT_DATE_FOR_PAGE = "dd.MM"; 
	public static final String FORMAT_DATE_FOR_SQL = "yyyy-MM-dd"; 
	
	
}
