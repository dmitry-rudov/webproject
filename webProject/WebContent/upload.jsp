
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Jee project for EPAM</title>
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<script src="../bootstrap/js/bootstrap.min.js"> </script>

		<div class="container">
			<%@ include file="header.jsp"%>

			<c:if test="${not empty error}">
				<div class="alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Ahtung!</strong> ${error}.
				</div>				
			</c:if>

			<form class="form-horizontal" name="Upload" method="POST" action="<c:url value='/operation/upload'/>" enctype="multipart/form-data">
  				<div class="control-group">
    				<div class="controls">
						<input type="file" name="FileName">
					</div>
				</div>
				<div class="control-group">
    				<div class="controls">
						<button class="btn btn-primary" name="Upload" type="submit" value="<c:out value="${number_task}"/>">Upload</button>
					</div>
				</div>
			</form>
	
			<form class="form-horizontal" name="Cancel" method="POST" action="<c:url value='/operation/addtask'/>">
				<div class="control-group">
    				<div class="controls">
						<button class="btn" name="Cancel" type="submit" value="Cancel">Cancel</button>
					</div>
				</div>
			</form>
	
			<%@ include file="footer.html"%>
		</div>

	</body>
</html>

