
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Jee project for EPAM</title>
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<script src="../bootstrap/js/bootstrap.min.js"> </script>

		<p>
			<c:if test="${not empty error}">
				${error}
			</c:if>
		</p>


		<div class="container">
			<%@ include file="header.jsp"%>

			<form class="form-horizontal" name="AddTask" method="POST" action="<c:url value='/operation/addtask'/>">
				
				<c:if test="${tab_sheet eq 'Today' || tab_sheet eq 'Tomorrow'}">
	  				<div class="control-group">
    					<div class="controls">
							<b> <c:out value="${date}"/> </b>
						</div>							
					</div>
				</c:if>
				
  				<div class="control-group">
    				<div class="controls">
						Enter a new task. No more than 256 characters <br>
						<textarea rows="4" cols="65" name="Task"></textarea>
						<br>
					</div>
				</div>
						
				<c:if test="${tab_sheet eq 'Someday'}">
		    		<div class="control-group">
    					<div class="controls">
		    				Enter the date for the task in the format: yyyy-MM-dd
							<input type="date" name="Calendar"> <br>
						</div>
					</div>
				</c:if>
			
  				<div class="control-group">
   					<div class="controls">
	  					<button type="submit" class="btn btn-primary" name="Add" value="Add">Add</button>
  						<button type="submit" class="btn" name="Cancel" value="Cancel">Cancel</button>
  					</div>
				</div>
			</form>
	
			<%@ include file="footer.html"%>
		</div>
	</body>
</html>