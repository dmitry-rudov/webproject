<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>

<%@ taglib uri="/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<script src="../bootstrap/js/bootstrap.min.js"> </script>
		<script src="bootstrap/js/bootstrap.min.js"> </script>

		<div class="container">
			<%@ include file="header.jsp"%>

			<%@ include file="form_chapters.jsp"%>

			<c:if test="${not empty error}">
				<div class="alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Ahtung!</strong> ${error}.
				</div>				
			</c:if>

			<c:if test="${tab_sheet eq 'Today' || tab_sheet eq 'Tomorrow'}">
				<b> <c:out value="${date}"/> </b>
			</c:if>

			<form name="Actions" method="POST" action="<c:url value='/operation/action'/>">
	
				<c:if test="${not empty tab_sheet}">
					<table border="2" width=90% cellpadding="5" cellspacing="0" align="center">
  						<tr>
	    					<td width="200">File</td>
    				
	    					<c:if test="${tab_sheet eq 'Someday'}">
		    					<td width="150">Date</td>
	    					</c:if>
	    				
	    					<td>Task</td>
    						<td width="25">Check</td>
  						</tr>
  				
	  					<c:forEach items="${tasks}" var="item" varStatus="status">
		 					<tr>
			    				<c:if test="${not empty item.file}">
							        <td width="200"> 
						        	<b> <c:out value="${item.file}"/> </b> <br>
									<button class="btn btn-success" name="Download" type="submit" value="<c:out value="${item.id}"/>">Download</button>
									<button class="btn btn-danger"  name="Delete file" type="submit" value="<c:out value="${item.id}"/>">Delete</button>
						    	    </td>
	    						</c:if>

    							<c:if test="${empty item.file}">
					        		<td width="200"> <button class="btn btn-primary" name="Upload" type="submit" value="<c:out value="${item.id}"/>">Upload</button> </td> 
		    					</c:if>
    				
    							<c:if test="${tab_sheet eq 'Someday'}">
			    					<td width="150"> <c:out value="${item.date}"/> </td>
			    				</c:if>
	    			
						        <td> <c:out value="${item.task}"/> </td>
						        <td width="25"> <input type="checkbox" name="task" value="<c:out value="${item.id}"/>"> </td>
					        </tr>
						</c:forEach>

					</table>
				</c:if>

				<%@ include file="action.jsp"%>
	
			</form>

			<%@ include file="footer.html"%>
		</div>
	
	</body>
</html>