<%@ page import="by.gsu.epamlab.constants.Constants" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<script src="bootstrap/js/bootstrap.min.js"> </script>

		<div class="container">
			<%@ include file="header.jsp"%>

			<c:if test="${not empty error}">
				<div class="alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Ahtung!</strong> ${error}.
				</div>				
			</c:if>

			<form class="form-horizontal" action = "<c:url value='/login'/>" method = "POST">
 				<div class="control-group">
    				<label class="control-label" for="inputLogin">Login</label>
    				<div class="controls">
      					<input type="text" id="inputLogin" placeholder="Login" name = "login">
    				</div>
  				</div>
  				<div class="control-group">
    				<label class="control-label" for="inputPassword">Password</label>
    				<div class="controls">
      					<input type="password" id="inputPassword" placeholder="Password" name="password">
    				</div>
  				</div>
  				<div class="control-group">
    				<div class="controls">
      					<button type="submit" class="btn" name = "enter" value="enter">Enter</button>
    				</div>
  				</div>
			</form>	

			<%@ include file="footer.html"%>
		</div>
		
	</body>
</html>