<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/jstl/core" prefix="c"%>
    
	<form name="Chapters" method="GET" action="<c:url value='/operation/task'/>">
	
		<c:choose>

			<c:when test="${tab_sheet eq 'Today'}">
				<b>Today ${date_today}</b>
				<input type="submit" name="Chapter" value="Tomorrow">
				<input type="submit" name="Chapter" value="Someday">
				<input type="submit" name="Chapter" value="Fix">
				<input type="submit" name="Chapter" value="Recycle Bin">
			</c:when>
	
			<c:when test="${tab_sheet eq 'Tomorrow'}">
				<input type="submit" name="Chapter" value="Today">
				<b>Tomorrow ${date_tomorrow}</b>
				<input type="submit" name="Chapter" value="Someday">
				<input type="submit" name="Chapter" value="Fix">
				<input type="submit" name="Chapter" value="Recycle Bin">
			</c:when>

			<c:when test="${tab_sheet eq 'Someday'}">
				<input type="submit" name="Chapter" value="Today">
				<input type="submit" name="Chapter" value="Tomorrow">
				<b>Someday</b>
				<input type="submit" name="Chapter" value="Fix">
				<input type="submit" name="Chapter" value="Recycle Bin">
			</c:when>

			<c:when test="${tab_sheet eq 'Fix'}">
				<input type="submit" name="Chapter" value="Today">
				<input type="submit" name="Chapter" value="Tomorrow">
				<input type="submit" name="Chapter" value="Someday">
				<b>Fix</b>
				<input type="submit" name="Chapter" value="Recycle Bin">
			</c:when>

			<c:when test="${tab_sheet eq 'Recycle Bin'}">
				<input type="submit" name="Chapter" value="Today">
				<input type="submit" name="Chapter" value="Tomorrow">
				<input type="submit" name="Chapter" value="Someday">
				<input type="submit" name="Chapter" value="Fix">
				<b>Recycle Bin</b>
			</c:when>

			<c:otherwise>
				<input type="submit" name="Chapter" value="Today">
				<input type="submit" name="Chapter" value="Tomorrow">
				<input type="submit" name="Chapter" value="Someday">
				<input type="submit" name="Chapter" value="Fix">
				<input type="submit" name="Chapter" value="Recycle Bin">
			</c:otherwise>
		
		</c:choose>
	
	</form>
