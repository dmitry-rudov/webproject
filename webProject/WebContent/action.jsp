<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/jstl/core" prefix="c"%>

		<c:choose>

			<c:when test="${tab_sheet eq 'Recycle Bin'}">
				<input type="submit" name="Action" value="Clear all">
				<input type="submit" name="Action" value="Delete">
				<input type="submit" name="Action" value="Recover">
			</c:when>

			<c:when test="${tab_sheet eq 'Fix'}">
				<input type="submit" name="Action" value="Remove">
			</c:when>

			<c:when test="${tab_sheet eq 'Today' || tab_sheet eq 'Tomorrow' || tab_sheet eq 'Someday'}">
				<input type="submit" name="Action" value="Add task">
				<input type="submit" name="Action" value="Fix">
				<input type="submit" name="Action" value="Remove">
			</c:when>

			<c:otherwise>
				Select chapter
			</c:otherwise>
		
		</c:choose>
