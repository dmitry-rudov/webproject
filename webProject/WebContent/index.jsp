<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Jee project for EPAM</title>
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<script src="bootstrap/js/bootstrap.min.js"> </script>

		<c:if test="${not empty user}">
			<c:redirect url="/operation/task"/>
		</c:if>

		<div class="container"> 
			<%@ include file="header.jsp"%>
			<h2 >Begin project</h2>
			<%@ include file="footer.html"%>
		</div>
	
	</body>
</html>