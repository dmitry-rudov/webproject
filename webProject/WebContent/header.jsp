<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>

<%@ taglib uri="/jstl/core" prefix="c"%>


	<c:choose>
	
		<c:when test="${not empty user}">
			<form name="logout" method="POST" action="<c:url value='/logout'/>">
				<b>User: ${user.login}</b>
				<button class="btn" name="logout" type="submit" value="logout">Logout</button>
			</form>
		</c:when>
	
		<c:otherwise>
			<b>User: Guest</b>
			<a href="<c:url value='/login'/>">Login</a>
			<a href="<c:url value='/registrate'/>">Registrate</a>
		</c:otherwise>
		
	</c:choose>
